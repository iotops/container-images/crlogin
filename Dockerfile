FROM ubuntu
RUN apt update ;\
    apt install -y python3-pip
RUN pip3 install awscli
ADD generateDockerConfigFile.sh /usr/local/bin/generateDockerConfigFile
RUN chmod a+x /usr/local/bin/generateDockerConfigFile
ENTRYPOINT /usr/local/bin/generateDockerConfigFile
