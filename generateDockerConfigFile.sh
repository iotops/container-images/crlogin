#!/bin/bash

DOCKER_CONFIG=${DOCKER_CONFIG:-.}
DH_REGISTRY=${DH_REGISTRY:-https://index.docker.io/v1/}

mkdir -p ${DOCKER_CONFIG}

cat << EOF > ${DOCKER_CONFIG}/config.json
{
    "auths": {
EOF

if [[ ! -z "${AWS_REGION}" ]] ; then

    command=(`aws ecr get-login --no-include-email --region ${AWS_REGION} | sed 's/ /\n/gm'`)

    AUTH=`echo -n "${command[3]}:${command[5]}" | base64 -w 0`

    cat << EOF >> ${DOCKER_CONFIG}/config.json
        "${command[6]}": {
            "auth": "${AUTH}",
            "email": ""
        }
EOF

    ADDCOMMA=true
fi

if [[ ! -z "${DH_LOGIN}" ]] ; then

    AUTH=`echo -n "${DH_LOGIN}:${DH_SECRET}" | base64 -w 0`

    if [[ ! -z "${ADDCOMMA}" ]] ; then
	cat << EOF >> ${DOCKER_CONFIG}/config.json
        ,
EOF

    fi

    cat << EOF >> ${DOCKER_CONFIG}/config.json
        "${DH_REGISTRY}": {
            "auth": "${AUTH}",
            "email": ""
        }

EOF
    ADDCOMMA=true
fi

cat << EOF >> ${DOCKER_CONFIG}/config.json
    }
}
EOF
